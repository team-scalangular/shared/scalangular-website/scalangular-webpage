#!/usr/bin/env bash

#try to get the stack id
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "X-Api-Key: $PORTAINER_API_KEY")

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$CI_ENVIRONMENT_SLUG'") | .Id')

if [ -z "$stackId" ]
then
  echo "Something went wrong while locating the stackId. Please delete it manually!"
  exit 1;
else
  curl --location --request DELETE "portainer.scalangular.com/api/stacks/$stackId?external=true&endpointId=2" \
  --header "X-Api-Key: $PORTAINER_API_KEY"
  exit 0;
fi
