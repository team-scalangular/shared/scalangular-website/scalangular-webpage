#!/usr/bin/env bash

echo "Commit slug: "
echo "$CI_COMMIT_REF_SLUG"

# Get compose yaml configurations
export STACK_NAME="$CI_ENVIRONMENT_SLUG"
export IMAGE_NAME="$PROJECT_NAME-$CI_COMMIT_REF_SLUG"
export DOMAIN_NAME="\`$CI_ENVIRONMENT_SLUG.scalangular.com\`"

( echo "cat <<EOF >final.yaml";
  cat ./scripts/docker_compose.template.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml

echo "Compose YAML is created. Deploying the stack with the previous configurations"
sleep 5s

#check if the stack already exists
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "X-Api-Key: $PORTAINER_API_KEY")

echo $json

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$STACK_NAME'") | .Id')

if [ -z "$stackId" ]
then
  echo "No stack deployed: creating a new one."
else
  echo "Review-Stack already deployed to stackId: $stackId"
  echo "Stack will be deleted before creating a new one"
  curl --location --request DELETE "portainer.scalangular.com/api/stacks/$stackId" \
  --header "X-Api-Key: $PORTAINER_API_KEY"
  sleep 10s
fi

#deploy the stack
curl --location --request POST 'portainer.scalangular.com/api/stacks?type=1&method=file&endpointId=2' \
--header "X-Api-Key: $PORTAINER_API_KEY" \
--form "Name=$STACK_NAME" \
--form "EndpointID='2'" \
--form "SwarmID=$REVIEW_SWARM" \
--form 'file=@"./final.yaml"' | jq .

sleep 5s

#verify deployment
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "X-Api-Key: $PORTAINER_API_KEY")

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$STACK_NAME'") | .Id')

if [ -z "$stackId" ]
then
  echo "Something went wrong deploying the stack."
  exit 1;
else
  echo "Review-Stack deployed to stackId: $stackId"
  exit 0;
fi
