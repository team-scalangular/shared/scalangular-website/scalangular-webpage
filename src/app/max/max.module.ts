import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaxRouting } from './max.routing';
import { PresentationComponent } from './presentation/presentation.component';
import { AboutComponent } from './about/about.component';
import { HomeModule } from '../home/home.module';

@NgModule({
  declarations: [PresentationComponent, AboutComponent],
  imports: [CommonModule, MaxRouting, HomeModule],
})
export class MaxModule {}
