import { Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss'],
})
export class PresentationComponent implements OnInit, OnDestroy {
  isBrowser = false;
  animationBlockInterval: ReturnType<typeof setInterval>;

  constructor(@Inject(PLATFORM_ID) private platform: any) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
      let lineCount = 1;
      this.animationBlockInterval = setInterval(() => {
        document.getElementById(lineCount.toLocaleString()).classList.add('tw__finished');
        lineCount = lineCount + 1;
        if (lineCount > 2) {
          clearInterval(this.animationBlockInterval);
        }
      }, 5000);
    }
  }

  ngOnDestroy(): void {
    if (this.animationBlockInterval) {
      clearInterval(this.animationBlockInterval);
    }
  }
}
