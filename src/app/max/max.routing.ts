import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PresentationComponent } from './presentation/presentation.component';
import { AboutComponent } from './about/about.component';

export const routes: Routes = [
  { path: '', component: PresentationComponent, data: { animationState: 'One' } },
  { path: 'about', component: AboutComponent, data: { animationState: 'Two' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaxRouting {}
