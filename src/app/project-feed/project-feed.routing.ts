import { RouterModule, Routes } from '@angular/router';
import { UnderConstructionComponent } from '../shared/components/under-construction/under-construction.component';
import { NgModule } from '@angular/core';
import { FeedComponent } from './feed/feed.component';

export const routes: Routes = [{ path: '', component: FeedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectFeedRouting {}
