import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProjectContributors } from '../../../../shared/interfaces/project-contributors';

@Component({
  selector: 'pf-feed-header',
  templateUrl: './feed-header.component.html',
  styleUrls: ['./feed-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedHeaderComponent implements OnInit {
  @Input() projectTitle = '';
  @Input() projectContributors: ProjectContributors[] = [];
  @Input() otherContributors: string[] | null;
  maxIsContributor: boolean;
  moriIsContributor: boolean;

  ngOnInit(): void {
    this.projectContributors.forEach((contributor) => {
      if (contributor === ProjectContributors.MAX) {
        this.maxIsContributor = true;
      } else {
        this.moriIsContributor = true;
      }
    });
  }
}
