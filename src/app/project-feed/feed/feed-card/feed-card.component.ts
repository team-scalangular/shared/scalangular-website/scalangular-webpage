import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ProjectContributors } from '../../../shared/interfaces/project-contributors';
import { NgxGalleryImage } from 'ngx-gallery-9';

@Component({
  selector: 'pf-feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedCardComponent {
  @Input() projectContributors: ProjectContributors[] = [];
  @Input() otherContributors: string[] | null;
  @Input() projectTitle = '';
  @Input() galleryImages: NgxGalleryImage[] | null;
}
