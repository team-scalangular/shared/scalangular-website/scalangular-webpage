import { Component, Input, OnInit } from '@angular/core';
import {
  NgxGalleryAnimation,
  NgxGalleryImage,
  NgxGalleryImageSize,
  NgxGalleryOptions,
} from 'ngx-gallery-9';

@Component({
  selector: 'pf-feed-images',
  templateUrl: './feed-images.component.html',
  styleUrls: ['./feed-images.component.scss'],
})
export class FeedImagesComponent implements OnInit {
  @Input() galleryImages: NgxGalleryImage[] | null;
  galleryOptions: NgxGalleryOptions[];

  ngOnInit(): void {
    this.galleryOptions = [
      {
        width: '100%',
        height: '15rem',
        thumbnailsColumns: 4,
        image: false,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageSize: NgxGalleryImageSize.Cover,
        thumbnailSize: NgxGalleryImageSize.Contain,
        previewCloseOnEsc: true,
        previewCloseOnClick: true,
        previewFullscreen: false,
      },
      {
        breakpoint: 800,
        imagePercent: 80,
      },
      {
        breakpoint: 600,
        height: '10rem',
      },
      {
        breakpoint: 400,
        height: '5rem',
      },
    ];
  }
}
