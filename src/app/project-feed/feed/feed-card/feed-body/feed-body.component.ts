import { Component, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'pf-feed-body',
  templateUrl: './feed-body.component.html',
  styleUrls: ['./feed-body.component.scss'],
})
export class FeedBodyComponent {
  @Input() hasImages: boolean;
}
