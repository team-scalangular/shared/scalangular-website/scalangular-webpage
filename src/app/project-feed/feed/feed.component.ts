import { Component, OnInit } from '@angular/core';
import { ProjectContributors } from '../../shared/interfaces/project-contributors';
import { NgxGalleryImage } from 'ngx-gallery-9';
import { ScrollProgressService } from '../../core/services/scroll-progress.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit {
  contributorsMaxAndMori: ProjectContributors[] = [
    ProjectContributors.MAX,
    ProjectContributors.MORI,
  ];
  contributorsMori: ProjectContributors[] = [ProjectContributors.MORI];
  contributorsMax: ProjectContributors[] = [ProjectContributors.MAX];

  right = 'col-12 col-xl-6 mb-5 mb-xl-0 mr-auto';
  left = 'col-12 col-xl-6 mb-5 mb-xl-0 ml-auto';

  galleryImages = [
    {
      small: 'assets/images/about/flutter.png',
      big: 'assets/images/about/flutter.png',
    },
    {
      small: 'assets/images/about/img7.jpg',
      big: 'assets/images/about/img7.jpg',
    },
    {
      small: 'assets/images/about/flutter.png',
      big: 'assets/images/about/flutter.png',
    },
    {
      small: 'assets/images/about/img7.jpg',
      big: 'assets/images/about/img7.jpg',
    },
    {
      small: 'assets/images/about/flutter.png',
      big: 'assets/images/about/flutter.png',
    },
  ];

  blockfightImages = [
    {
      small: 'assets/images/project-feed/blockfight/GamePlay.gif',
      big: 'assets/images/project-feed/blockfight/GamePlay.gif',
    },
    {
      small: 'assets/images/project-feed/blockfight/ArrowDemo.gif',
      big: 'assets/images/project-feed/blockfight/ArrowDemo.gif',
    },
    {
      small: 'assets/images/project-feed/blockfight/Hit.gif',
      big: 'assets/images/project-feed/blockfight/Hit.gif',
    },
    {
      small: 'assets/images/project-feed/blockfight/PortalDemoBehaviour_old.gif',
      big: 'assets/images/project-feed/blockfight/PortalDemoBehaviour_old.gif',
    },
    {
      small: 'assets/images/project-feed/blockfight/Shader.gif',
      big: 'assets/images/project-feed/blockfight/Shader.gif',
    },
  ];

  f1V2Images = [
    {
      big: 'assets/images/project-feed/f1v2/F1V2_Screenshot1.PNG',
      small: 'assets/images/project-feed/f1v2/F1V2_Screenshot1.PNG',
      description: 'View of all the race results for the race in azerbaijan',
    },
    {
      big: 'assets/images/project-feed/f1v2/F1V2_Screenshot2.PNG',
      small: 'assets/images/project-feed/f1v2/F1V2_Screenshot2.PNG',
      description: 'View of a drivers race results',
    },
  ];

  zkpfrImages: NgxGalleryImage[] = [
    {
      small: 'assets/images/project-feed/zkpfr-home.png',
      big: 'assets/images/project-feed/zkpfr-home.png',
      description: 'Home view when logged in whit all the channels',
    },
    {
      small: 'assets/images/project-feed/zkpfr-login.png',
      big: 'assets/images/project-feed/zkpfr-login.png',
      description: 'Register screen',
    },
    {
      small: 'assets/images/project-feed/zkpfr-chat.png',
      big: 'assets/images/project-feed/zkpfr-chat.png',
      description: 'Chat window for the user interaction',
    },
    {
      small: 'assets/images/project-feed/zkpfr-account.png',
      big: 'assets/images/project-feed/zkpfr-account.png',
      description: 'Account Settings Menu',
    },
  ];

  deManImages: NgxGalleryImage[] = [
    {
      small: 'assets/images/project-feed/deman-register.PNG',
      big: 'assets/images/project-feed/deman-register.PNG',
      description: 'Registration view for the application',
    },
    {
      small: 'assets/images/project-feed/deman-overview.PNG',
      big: 'assets/images/project-feed/deman-overview.PNG',
      description: 'Transaction overview between two users',
    },
    {
      small: 'assets/images/project-feed/deman-sidenav.PNG',
      big: 'assets/images/project-feed/deman-sidenav.PNG',
      description: 'Side navigation',
    },
    {
      small: 'assets/images/project-feed/deman-create-transaction.PNG',
      big: 'assets/images/project-feed/deman-create-transaction.PNG',
      description: 'View to create new transactions',
    },
  ];

  constructor(private scrollProgress: ScrollProgressService) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.scrollProgress.scrollIsBlocked.next(true);
    setTimeout(() => this.scrollProgress.scrollIsBlocked.next(false), 3000);
  }
}
