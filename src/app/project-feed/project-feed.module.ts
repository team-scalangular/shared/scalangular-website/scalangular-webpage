import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedComponent } from './feed/feed.component';
import { FeedImagesComponent } from './feed/feed-card/feed-images/feed-images.component';
import { FeedBodyComponent } from './feed/feed-card/feed-body/feed-body.component';
import { FeedHeaderComponent } from './feed/feed-card/feed-header/feed-header.component';
import { ProjectFeedRouting } from './project-feed.routing';
import { FeedCardComponent } from './feed/feed-card/feed-card.component';
import { NgxGalleryModule } from 'ngx-gallery-9';
import { HomeModule } from '../home/home.module';

@NgModule({
  declarations: [
    FeedComponent,
    FeedImagesComponent,
    FeedBodyComponent,
    FeedHeaderComponent,
    FeedCardComponent,
  ],
  imports: [CommonModule, ProjectFeedRouting, NgxGalleryModule, HomeModule],
})
export class ProjectFeedModule {}
