import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'maxe', loadChildren: () => import('./max/max.module').then((m) => m.MaxModule) },
  {
    path: 'moritz',
    loadChildren: () => import('./moritz/moritz.module').then((m) => m.MoritzModule),
  },
  {
    path: 'project-feed',
    loadChildren: () =>
      import('./project-feed/project-feed.module').then((m) => m.ProjectFeedModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      initialNavigation: 'enabledNonBlocking',
      onSameUrlNavigation: 'reload',
      preloadingStrategy: PreloadAllModules,
      useHash: false,
      enableTracing: false,
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRouting {}
