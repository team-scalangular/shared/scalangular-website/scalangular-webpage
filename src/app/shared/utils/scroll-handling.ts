import { Observable, of } from 'rxjs';

export function blockOrCalculateScrollProgress([, , scrollIsBlocked]: [
  never,
  never,
  boolean
]): void {
  if (scrollIsBlocked) {
    window.scrollTo(0, 0);
  } else {
    const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    const scrolled = (winScroll / height) * 100;
    document.getElementById('progressbar').style.height = `${scrolled}%`;
  }
}

export function resetScrollProcess(): void {
  document.getElementById('progressbar').style.height = '0';
}

export function isEnabledOrBlocked([, scrollIsEnabled, scrollIsBlocked]: [
  never,
  boolean,
  boolean
]): boolean {
  return scrollIsEnabled || scrollIsBlocked;
}

export function toVoid(): Observable<void> {
  return of(void 0);
}
