import { NavigationEnd, NavigationStart, RouterEvent } from '@angular/router';

export function isNavigationStart(event: RouterEvent): event is NavigationStart {
  return event instanceof NavigationStart;
}

export function isNavigationEnd(event: RouterEvent): event is NavigationEnd {
  return event instanceof NavigationEnd;
}
