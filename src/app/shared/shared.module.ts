import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LegalComponent } from './components/legal/legal.component';
import { SharedRouting } from './shared.routing';
import { UnderConstructionComponent } from './components/under-construction/under-construction.component';
import { MainLogoComponent } from './components/main-logo/main-logo.component';
import { HomeModule } from '../home/home.module';

@NgModule({
  declarations: [LegalComponent, UnderConstructionComponent, MainLogoComponent],
  imports: [CommonModule, SharedRouting, HomeModule],
  exports: [MainLogoComponent],
})
export class SharedModule {}
