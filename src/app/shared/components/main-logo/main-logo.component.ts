import { Component } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { RouterEventsService } from '../../../core/services/router-events.service';

@Component({
  selector: 'shared-main-logo',
  templateUrl: './main-logo.component.html',
  styleUrls: ['./main-logo.component.scss'],
})
export class MainLogoComponent {
  currentRoute$: Observable<NavigationEnd> = this.routerEvents.navigationEnd$;

  urlsForRedLogo: string[] = ['/maxe/about'];
  urlsForTransparentBackground: string[] = ['/moritz'];

  constructor(private routerEvents: RouterEventsService) {}

  getLogoPath(event: NavigationEnd): string {
    return this.urlsForRedLogo.includes(event.url)
      ? 'assets/images/logo/red-logo.png'
      : 'assets/images/logo/logo_transparent.png';
  }
}
