import { Component } from '@angular/core';

@Component({
  selector: 'shared-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
})
export class LegalComponent {}
