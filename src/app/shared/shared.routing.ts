import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LegalComponent } from './components/legal/legal.component';

const routes: Routes = [{ path: 'legal', component: LegalComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharedRouting {}
