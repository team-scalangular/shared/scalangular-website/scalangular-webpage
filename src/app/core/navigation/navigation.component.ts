import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { RouterEventsService } from '../services/router-events.service';

@Component({
  selector: 'core-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  isChecked = false;

  constructor(@Inject(PLATFORM_ID) private platformId, private routerEvents: RouterEventsService) {}

  ngOnInit(): void {
    this.routerEvents.navigationEnd$.subscribe(() => this.handleNavigationEnd());
  }

  onChange(): void {
    this.isChecked = !this.isChecked;
  }

  private handleNavigationEnd(): void {
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
      this.isChecked = false;
    }
  }
}
