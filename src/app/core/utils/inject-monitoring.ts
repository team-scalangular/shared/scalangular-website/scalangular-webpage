export function injectMonitoringScripts(): void {
  const analyticsMinJs = document.createElement('script');
  analyticsMinJs.type = 'text/javascript';
  analyticsMinJs.src = 'assets/scripts/analytics-min.js';
  window.document.head.appendChild(analyticsMinJs);
  setTimeout(() => {
    const analyticsConf = document.createElement('script');
    analyticsConf.type = 'text/javascript';
    analyticsConf.src = 'assets/scripts/analytics-conf.js';
    window.document.head.appendChild(analyticsConf);
  }, 200);
}
