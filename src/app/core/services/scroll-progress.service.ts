import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';
import { filter, flatMap, tap, withLatestFrom } from 'rxjs/operators';
import {
  blockOrCalculateScrollProgress,
  isEnabledOrBlocked,
  toVoid,
} from '../../shared/utils/scroll-handling';

@Injectable({
  providedIn: 'root',
})
export class ScrollProgressService {
  scrollIsEnabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  scrollIsBlocked: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  scrollHandling$: Observable<void> = fromEvent(window, 'scroll').pipe(
    withLatestFrom(this.scrollIsEnabled, this.scrollIsBlocked),
    filter(isEnabledOrBlocked),
    tap(blockOrCalculateScrollProgress),
    flatMap(toVoid)
  );
}
