import { Injectable } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { isNavigationEnd, isNavigationStart } from '../../shared/utils/navigation-utils';

@Injectable({
  providedIn: 'root',
})
export class RouterEventsService {
  navigationStart$: Observable<NavigationStart> = this.router.events.pipe(
    filter(isNavigationStart)
  );
  navigationEnd$: Observable<NavigationEnd> = this.router.events.pipe(filter(isNavigationEnd));

  constructor(private router: Router) {}
}
