import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { ScrollComponent } from './scroll/scroll.component';

@NgModule({
  declarations: [NavigationComponent, ScrollComponent],
  imports: [CommonModule, BrowserModule, FormsModule, HttpClientModule, RouterModule],
  exports: [NavigationComponent, ScrollComponent],
})
export class CoreModule {}
