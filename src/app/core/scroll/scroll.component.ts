import { AfterViewInit, Component, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { skip } from 'rxjs/operators';
import { resetScrollProcess } from '../../shared/utils/scroll-handling';
import { RouterEventsService } from '../services/router-events.service';
import { ScrollProgressService } from '../services/scroll-progress.service';

@Component({
  selector: 'core-scroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.scss'],
})
export class ScrollComponent implements AfterViewInit {
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private routerEvents: RouterEventsService,
    private scrollProgress: ScrollProgressService
  ) {}

  ngAfterViewInit(): void {
    this.initializeScrollHandling();
  }

  private initializeScrollHandling(): void {
    this.scrollProgress.scrollHandling$.subscribe();
    this.routerEvents.navigationEnd$.pipe(skip(1)).subscribe(() => {
      resetScrollProcess();
    });
  }
}
