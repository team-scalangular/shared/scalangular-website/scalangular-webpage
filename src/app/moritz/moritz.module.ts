import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoritzRouting } from './moritz.routing';
import { PresentationComponent } from './presentation/presentation.component';
import { HomeModule } from '../home/home.module';

@NgModule({
  declarations: [PresentationComponent],
  imports: [CommonModule, MoritzRouting, HomeModule],
})
export class MoritzModule {}
