import { RouterModule, Routes } from '@angular/router';
import { UnderConstructionComponent } from '../shared/components/under-construction/under-construction.component';
import { NgModule } from '@angular/core';
import { PresentationComponent } from './presentation/presentation.component';

export const routes: Routes = [{ path: '', component: PresentationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoritzRouting {}
