import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Data, RouterOutlet } from '@angular/router';
import { slideInAnimation } from './shared/animations';
import { environment } from '../environments/environment';
import { Meta } from '@angular/platform-browser';
import { RouterEventsService } from './core/services/router-events.service';
import { isPlatformBrowser } from '@angular/common';
import { ScrollProgressService } from './core/services/scroll-progress.service';
import { injectMonitoringScripts } from './core/utils/inject-monitoring';

declare global {
  interface Window {
    strum: (eventType: string, url: string) => void;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation],
})
export class AppComponent implements OnInit {
  title = 'scalangular-webpage';
  showCustomScroll: boolean;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private universalDeviceDetector: DeviceDetectorService,
    private meta: Meta,
    private routerEvents: RouterEventsService,
    private scrollProgress: ScrollProgressService
  ) {}

  ngOnInit(): void {
    this.enableMonitoringOnProd();
    this.createCustomScrollbar();
  }

  private createCustomScrollbar(): void {
    const isChrome = this.universalDeviceDetector.getDeviceInfo().browser === 'Chrome';
    this.showCustomScroll = this.universalDeviceDetector.isDesktop() && isChrome;
    this.scrollProgress.scrollIsEnabled.next(this.showCustomScroll);
    if (this.showCustomScroll) {
      document.getElementsByTagName('body')[0].classList.add('custom-scroll');
    }
  }

  prepareRoute(outlet: RouterOutlet): Data {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animationState'];
  }

  private enableMonitoringOnProd(): void {
    if (environment.production && isPlatformBrowser(this.platformId)) {
      injectMonitoringScripts();
      this.watchRouteChanges();
    }
  }

  private watchRouteChanges(): void {
    this.routerEvents.navigationStart$.subscribe((event) => {
      window['strum']('routeChange', event.url);
    });
  }
}
