import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HomeRouting } from './home.routing';
import { IntroComponent } from './home/intro/intro.component';
import { AboutUsComponent } from './home/about-us/about-us.component';
import { FooterComponent } from './home/footer/footer.component';
import { AboutScalAngularComponent } from './home/about-scalangular/about-scal-angular.component';
import { FormsModule } from '@angular/forms';
import { ScrollHintComponent } from './home/intro/scroll-hint/scroll-hint.component';
import { CopyrightComponent } from './home/copyright/copyright.component';

@NgModule({
  declarations: [
    HomeComponent,
    IntroComponent,
    AboutUsComponent,
    FooterComponent,
    AboutScalAngularComponent,
    ScrollHintComponent,
    CopyrightComponent,
  ],
  exports: [FooterComponent],
  imports: [CommonModule, HomeRouting, FormsModule],
})
export class HomeModule {}
