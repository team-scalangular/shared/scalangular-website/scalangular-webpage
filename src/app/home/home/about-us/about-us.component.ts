import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'home-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
})
export class AboutUsComponent implements OnInit {
  textIsHoveredForMori: boolean;
  textIsHoveredForMax: boolean;

  constructor() {}

  ngOnInit(): void {
    this.textIsHoveredForMax = false;
    this.textIsHoveredForMori = false;
  }

  /*
    Target is either 'Max' or 'Moritz
   */
  onHover(target: string): void {
    if (target === 'Moritz') {
      this.textIsHoveredForMori = true;
    } else {
      this.textIsHoveredForMax = true;
    }
  }

  onMouseLeave(target: string): void {
    if (target === 'Moritz') {
      this.textIsHoveredForMori = false;
    } else {
      this.textIsHoveredForMax = false;
    }
  }
}
