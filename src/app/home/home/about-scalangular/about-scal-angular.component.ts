import { Component, HostListener, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'home-about-scalangular',
  templateUrl: './about-scal-angular.component.html',
  styleUrls: ['./about-scal-angular.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate('1s ease-out', style({ opacity: 1 }))]),
      transition(':leave', [style({ opacity: 1 }), animate('1s ease-in', style({ opacity: 0 }))]),
    ]),
  ],
})
export class AboutScalAngularComponent implements OnInit, OnDestroy {
  images: string[] = [
    'url("../../../../assets/images/about/Ma.jpeg")',
    'url("../../../../assets/images/about/Mo.jpg")',
    'url("../../../../assets/images/about/angular.png")',
    'url("../../../../assets/images/about/docker.png")',
    'url("../../../../assets/images/about/scala.png")',
    'url("../../../../assets/images/about/ts-logo.png")',
    'url("../../../../assets/images/about/kotlin.png")',
    'url("../../../../assets/images/about/flutter.png")',
    'url("../../../../assets/images/about/angularrx.png")',
    'url("../../../../assets/images/about/unity.png")',
    'url("../../../../assets/images/about/postgress.png")',
    'url("../../../../assets/images/about/scala-js.png")',
    'url("../../../../assets/images/about/fmodelling.png")',
    'url("../../../../assets/images/about/ngrx.png")',
    'url("../../../../assets/images/about/mongo.png")',
  ];
  timer: ReturnType<typeof setInterval>;
  screenWidth: number;
  imageIndex = 0;
  loading = true;
  animationDisabled = true;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {}

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.screenWidth = window.innerWidth;
    this.clearOrCreateInterval();
  }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
    this.updateAnimation();
  }

  updateAnimation(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.loading = false;
      this.clearOrCreateInterval();
    }
  }

  private clearOrCreateInterval(): void {
    if (this.screenWidth > 1200 && !this.animationDisabled) {
      this.timer = setInterval(this.createAnimatedImage.bind(this), 180);
    } else if (this.timer && this.animationDisabled) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  createAnimatedImage(): void {
    const section = document.getElementById('team-tornado');
    const drop = document.createElement('span');

    drop.style.top = Math.random() * (window.innerHeight - 200) + 'px';

    const nextImageToRender = this.imageIndex;
    this.imageIndex = this.imageIndex >= this.images.length ? 0 : this.imageIndex + 1;
    const currentImage = this.images[nextImageToRender];

    const defaultSize = 150;

    if (nextImageToRender === 4) {
      // scala logo needs to be a little smaller
      drop.style.width = 86 + 'px';
    } else {
      drop.style.width = defaultSize + 'px';
    }

    drop.style.height = defaultSize + 'px';
    drop.style.backgroundImage = currentImage;
    drop.classList.add('span-style');

    section.appendChild(drop);

    setTimeout(() => {
      drop.remove();
    }, 6000);
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }
}
