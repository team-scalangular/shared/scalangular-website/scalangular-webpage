import { Component, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'home-scroll-hint',
  templateUrl: './scroll-hint.component.html',
  styleUrls: ['./scroll-hint.component.scss'],
  // source: https://medium.com/ngconf/animating-angulars-ngif-and-ngfor-32a6ff26ed2d
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('1s ease-out', style({ height: 300, opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: 300, opacity: 1 }),
        animate('1s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
@UntilDestroy()
export class ScrollHintComponent implements OnInit {
  showScrollHint = true;

  constructor(private router: Router, private viewPortScroller: ViewportScroller) {}

  ngOnInit(): void {
    fromEvent(window, 'scroll')
      .pipe(
        untilDestroyed(this),
        debounceTime(100),
        tap(() => this.handleScroll())
      )
      .subscribe();
  }

  handleScroll(): void {
    this.showScrollHint = window.scrollY <= 200;
  }

  scrollToAnchor(): void {
    document.documentElement.style.scrollBehavior = 'smooth';
    this.viewPortScroller.scrollToAnchor('team-tornado');
    document.documentElement.style.scrollBehavior = null;
  }
}
