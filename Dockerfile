FROM node:16-alpine as buildContainer
WORKDIR /app
COPY ./package.json ./package-lock.json /app/
RUN npm ci
COPY . /app
RUN npm run build:ssr

FROM node:16-alpine
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /app

COPY --from=buildContainer /app/package.json /app
COPY --from=buildContainer /app/dist /app/dist

EXPOSE 4000

CMD ["npm", "run", "serve:ssr"]

RUN adduser -D myuser
USER myuser
